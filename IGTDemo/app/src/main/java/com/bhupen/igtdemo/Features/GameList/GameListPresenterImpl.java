package com.bhupen.igtdemo.Features.GameList;

import android.content.Context;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.DataTypes.Games;
import com.bhupen.igtdemo.DataTypes.PlayerInfo;
import com.bhupen.igtdemo.IGTApp;
import com.bhupen.igtdemo.Shared.API.ApiService;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class GameListPresenterImpl implements GameListPresenterInterface {

    private  GameListViewInterface view;
    @Inject
    ApiService apiService;


    public GameListPresenterImpl(Context context) {
        ((IGTApp)context).getAppComponent().inject(this);
    }

    @Override
    public void loadGames() {
        apiService.getGamesList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Games>() {
                    @Override
                    public void onCompleted() {
                        view.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(Games games) {
                        view.showGames(games);
                    }
                });
    }

    @Override
    public void loadPalyerInfo() {
        apiService.getPlayerInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<PlayerInfo>() {
                    @Override
                    public void onCompleted() {
                        view.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(PlayerInfo playerInfo) {
                        view.showPlayerInfo(playerInfo);
                    }
                });
    }

    @Override
    public void setView(GameListViewInterface view) {
        this.view = view;
    }

    @Override
    public void launchDetailsViews(Game game) {
        view.launchGameDetail(game);
    }

}
