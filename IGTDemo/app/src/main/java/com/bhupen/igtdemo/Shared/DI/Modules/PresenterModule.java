package com.bhupen.igtdemo.Shared.DI.Modules;

import android.content.Context;

import com.bhupen.igtdemo.Features.GameDetails.GameDetailsPresenterImpl;
import com.bhupen.igtdemo.Features.GameList.GameListPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    GameListPresenterImpl provideGameListPresenterImpl(Context context) {
        return new GameListPresenterImpl(context);
    }

    @Provides
    @Singleton
    GameDetailsPresenterImpl provideGameDetailsPresenterImpl(Context context) {
        return new GameDetailsPresenterImpl(context);
    }
}
