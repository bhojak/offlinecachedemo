package com.bhupen.igtdemo.DataTypes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Games implements Parcelable {

    public String response;
    public String currency;
    @SerializedName("data")
    public List<Game> data = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<Game> getData() {
        return data;
    }

    public void setData(List<Game> data) {
        this.data = data;
    }

    public final static Parcelable.Creator<Games> CREATOR = new Creator<Games>() {

        @SuppressWarnings({"unchecked"})
        public Games createFromParcel(Parcel in) {
            Games instance = new Games();
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.currency = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.data, (Game.class.getClassLoader()));
            return instance;
        }

        public Games[] newArray(int size) {
            return (new Games[size]);
        }

    }
            ;

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(response);
        dest.writeValue(currency);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}
