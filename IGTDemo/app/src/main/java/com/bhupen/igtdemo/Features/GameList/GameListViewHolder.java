package com.bhupen.igtdemo.Features.GameList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bhupen.igtdemo.R;


public class GameListViewHolder extends RecyclerView.ViewHolder {

    private TextView name;

    public GameListViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);
    }

    public TextView getName() {
        return name;
    }
}
