package com.bhupen.igtdemo.Shared.API;

import com.bhupen.igtdemo.DataTypes.Games;
import com.bhupen.igtdemo.DataTypes.PlayerInfo;

import retrofit2.http.GET;
import rx.Observable;

public interface ApiService {

    @GET("2ewt6r22zo4qwgx/gameData.json")
    Observable<Games> getGamesList();

    @GET("5zz3hibrxpspoe5/playerInfo.js")
    Observable<PlayerInfo> getPlayerInfo();
}
