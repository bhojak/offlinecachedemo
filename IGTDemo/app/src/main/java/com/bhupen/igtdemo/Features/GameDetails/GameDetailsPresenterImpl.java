package com.bhupen.igtdemo.Features.GameDetails;

import android.content.Context;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.IGTApp;


public class GameDetailsPresenterImpl implements GameDetailsPresenterInterface {

    private GameDetailsViewInterface view;

    public GameDetailsPresenterImpl(Context context) {
        ((IGTApp)context).getAppComponent().inject(this);
    }

    @Override
    public void setView(GameDetailsViewInterface view) {
        this.view = view;
    }

    @Override
    public void getGameDetails(Game game) {
        view.showGameDetails(game);
    }
}
