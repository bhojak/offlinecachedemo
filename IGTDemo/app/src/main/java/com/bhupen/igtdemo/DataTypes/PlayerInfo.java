package com.bhupen.igtdemo.DataTypes;


import android.os.Parcel;
import android.os.Parcelable;

public class PlayerInfo implements Parcelable{

    private String name;
    private Long balance;
    private String avatarLink;
    private String lastLogindate;

    public final static Parcelable.Creator<PlayerInfo> CREATOR = new Creator<PlayerInfo>() {


        @SuppressWarnings({"unchecked"})
        public PlayerInfo createFromParcel(Parcel in) {
            PlayerInfo instance = new PlayerInfo();
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.balance = ((Long) in.readValue((Long.class.getClassLoader())));
            instance.avatarLink = ((String) in.readValue((String.class.getClassLoader())));
            instance.lastLogindate = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public PlayerInfo[] newArray(int size) {
            return (new PlayerInfo[size]);
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    public String getLastLogindate() {
        return lastLogindate;
    }

    public void setLastLogindate(String lastLogindate) {
        this.lastLogindate = lastLogindate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(balance);
        dest.writeValue(avatarLink);
        dest.writeValue(lastLogindate);
    }

    public int describeContents() {
        return 0;
    }

}
