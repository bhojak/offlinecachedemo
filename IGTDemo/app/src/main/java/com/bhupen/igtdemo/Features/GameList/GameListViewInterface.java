package com.bhupen.igtdemo.Features.GameList;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.DataTypes.Games;
import com.bhupen.igtdemo.DataTypes.PlayerInfo;


public interface GameListViewInterface {

    void showGames(Games games);

    void showPlayerInfo(PlayerInfo playerInfo);

    void showError(String message);

    void showComplete();

    void launchGameDetail(Game game);
}
