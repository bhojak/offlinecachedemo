package com.bhupen.igtdemo;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bhupen.igtdemo.Shared.DI.Components.AppComponent;
import com.bhupen.igtdemo.Shared.DI.Components.DaggerAppComponent;
import com.bhupen.igtdemo.Shared.DI.Modules.AppModule;

import net.danlew.android.joda.JodaTimeAndroid;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class IGTApp extends Application {

    private AppComponent appComponent;
    private static IGTApp instance;

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = initDagger(this);
        JodaTimeAndroid.init(this);

        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfig);

    }

    protected AppComponent initDagger(IGTApp application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public static IGTApp getInstance () {
        return instance;
    }

    public static boolean hasNetwork () {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
