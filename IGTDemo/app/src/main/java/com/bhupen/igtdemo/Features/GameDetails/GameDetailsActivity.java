package com.bhupen.igtdemo.Features.GameDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.DataTypes.PlayerInfo;
import com.bhupen.igtdemo.IGTApp;
import com.bhupen.igtdemo.R;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by Bhupen on 22/06/2017.
 */

public class GameDetailsActivity extends AppCompatActivity implements GameDetailsViewInterface{

    private TextView name, jackpot, date;
    private TextView myName, balance;
    private ImageView avatar;
    private PlayerInfo playerInfo;
    private Game game;

    @Inject
     GameDetailsPresenterImpl presenter;

    public static final String EXTRA_GAME_DETAILS = "EXTRA_GAME_DETAILS";
    public static final String EXTRA_PLAYERS_INFO = "EXTRA_PLAYERS_INFO";

    public static void launch(Context context, Game game, PlayerInfo playerInfo) {
        Intent intent = new Intent(context, GameDetailsActivity.class);
        intent.putExtra(EXTRA_GAME_DETAILS, game);
        intent.putExtra(EXTRA_PLAYERS_INFO, playerInfo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ((IGTApp)getApplication()).getAppComponent().inject(this);

        ButterKnife.bind(this);
        game =  getIntent().getParcelableExtra(EXTRA_GAME_DETAILS);
        playerInfo = getIntent().getParcelableExtra(EXTRA_PLAYERS_INFO);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        presenter.setView(this);
        name = (TextView) findViewById(R.id.txt_name);
        jackpot = (TextView) findViewById(R.id.txt_jackpot);
        date = (TextView) findViewById(R.id.txt_date);
        initHeader();

       presenter.getGameDetails(game);
    }

    private void initHeader(){
        myName = (TextView) findViewById(R.id.txt_my_name);
        balance =(TextView) findViewById(R.id.txt_balance);

        avatar = (ImageView) findViewById(R.id.avatar);
        Picasso.with(this)
                .load(playerInfo.getAvatarLink())
                // .placeholder(DRAWABLE RESOURCE)   // optional
                //  .error(DRAWABLE RESOURCE)      // optional
                //  .resize(width, height)         // optional
                //  .rotate(degree)                // optional
                .into(avatar);
        myName.setText(playerInfo.getName());
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        balance.setText(format.format(playerInfo.getBalance()));

    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void showGameDetails(Game game) {
        name.setText(game.name);
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        jackpot.setText(format.format(game.jackpot));
        date.setText(formatDate(game.date.replace("+", ".000+")));

    }

    @Override
    public void showErrorMessage() {

    }

    private String formatDate (String dateIn)  {
        String result = "";
        try{
            String customFormat = "dd/MM/yyyy HH:mm";
            DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
            LocalDateTime parsedDate = dtf.parseLocalDateTime(dateIn);
            result = parsedDate.toString(DateTimeFormat.forPattern(customFormat));

        } catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
