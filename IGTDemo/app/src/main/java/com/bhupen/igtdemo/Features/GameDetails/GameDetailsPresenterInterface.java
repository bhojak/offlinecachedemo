package com.bhupen.igtdemo.Features.GameDetails;

import com.bhupen.igtdemo.DataTypes.Game;


public interface GameDetailsPresenterInterface {

    void setView(GameDetailsViewInterface view);

    void getGameDetails(Game game);
}
