package com.bhupen.igtdemo.Features.GameDetails;

import com.bhupen.igtdemo.DataTypes.Game;


public interface GameDetailsViewInterface {

    void showGameDetails(Game game);

    void showErrorMessage();
}
