package com.bhupen.igtdemo.Shared.DI.Components;

import com.bhupen.igtdemo.Features.GameDetails.GameDetailsActivity;
import com.bhupen.igtdemo.Features.GameDetails.GameDetailsPresenterImpl;
import com.bhupen.igtdemo.Features.GameList.GameListActivity;
import com.bhupen.igtdemo.Features.GameList.GameListPresenterImpl;
import com.bhupen.igtdemo.Shared.DI.Modules.AppModule;
import com.bhupen.igtdemo.Shared.DI.Modules.NetModule;
import com.bhupen.igtdemo.Shared.DI.Modules.PresenterModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, PresenterModule.class, NetModule.class})
public interface AppComponent {

    void inject(GameListActivity target);
    void inject(GameDetailsActivity target);
    void inject(GameListPresenterImpl target);
    void inject(GameDetailsPresenterImpl target);
}
