package com.bhupen.igtdemo.DataTypes;

import android.os.Parcel;
import android.os.Parcelable;


public class Game implements Parcelable {

    public String name;
    public Integer jackpot;
    public String date;
    public final static Parcelable.Creator<Game> CREATOR = new Creator<Game>() {

        @SuppressWarnings({"unchecked"})
        public Game createFromParcel(Parcel in) {
            Game instance = new Game();
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.jackpot = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.date = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Game[] newArray(int size) {
            return (new Game[size]);
        }

    };


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(jackpot);
        dest.writeValue(date);
    }

    public int describeContents() {
        return 0;
    }

}
