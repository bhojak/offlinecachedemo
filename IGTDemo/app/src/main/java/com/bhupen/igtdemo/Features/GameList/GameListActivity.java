package com.bhupen.igtdemo.Features.GameList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.DataTypes.Games;
import com.bhupen.igtdemo.DataTypes.PlayerInfo;
import com.bhupen.igtdemo.Features.GameDetails.GameDetailsActivity;
import com.bhupen.igtdemo.IGTApp;
import com.bhupen.igtdemo.R;
import com.bhupen.igtdemo.Shared.Interfaces.ClickListener;
import com.bhupen.igtdemo.Shared.Interfaces.RecyclerTouchListener;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GameListActivity extends AppCompatActivity implements GameListViewInterface{

    @BindView(R.id.activity_game_recyclerView)
    RecyclerView gamesRecyclerView;

    private LinearLayoutManager mLinearLayoutManager;
    private GameListAdapter adapter;
    private PlayerInfo playerInfo;
    private TextView name, balance, lastLogin;
    private ImageView avatar;

    @Inject
    GameListPresenterImpl presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((IGTApp)getApplication()).getAppComponent().inject(this);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();
        presenter.setView(this);
        presenter.loadGames();
        presenter.loadPalyerInfo();

        gamesRecyclerView = (RecyclerView) findViewById(R.id.activity_game_recyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        gamesRecyclerView.setLayoutManager(mLinearLayoutManager);

        adapter = new GameListAdapter();

        gamesRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gamesRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                    presenter.launchDetailsViews(adapter.getSelected(position));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // implement interface methods
    @Override
    public void showGames(Games games) {
        gamesRecyclerView.setAdapter(adapter);
        adapter.setGamesList(games);
    }

    @Override
    public void showPlayerInfo(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
        name = (TextView) findViewById(R.id.txt_my_name);
        balance =(TextView) findViewById(R.id.txt_balance);
        lastLogin =(TextView) findViewById(R.id.txt_last_login);
        avatar = (ImageView) findViewById(R.id.avatar);
        Picasso.with(this)
                .load(playerInfo.getAvatarLink())
               // .placeholder(DRAWABLE RESOURCE)   // optional
              //  .error(DRAWABLE RESOURCE)      // optional
              //  .resize(200, 200)         // optional
              //  .rotate(degree)                // optional
                .into(avatar);
        name.setText(playerInfo.getName());

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        balance.setText(format.format(playerInfo.getBalance()));
        lastLogin.setText(playerInfo.getLastLogindate().replace("T", "  "));
        lastLogin.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), "Error" + message, Toast.LENGTH_SHORT).show();

        Log.e("Error", message);
    }

    @Override
    public void showComplete() {
        Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void launchGameDetail(Game game) {
        GameDetailsActivity.launch(this, game, playerInfo);
    }
}
