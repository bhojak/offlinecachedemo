package com.bhupen.igtdemo.Features.GameList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhupen.igtdemo.DataTypes.Game;
import com.bhupen.igtdemo.DataTypes.Games;
import com.bhupen.igtdemo.R;

import java.util.Collections;
import java.util.List;


public class GameListAdapter extends RecyclerView.Adapter<GameListViewHolder> {

    private List<Game> gamesList;
    private Games games;


    public GameListAdapter() {
        this.gamesList = Collections.emptyList();
    }

    @Override
    public GameListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_games, null, false);
        return new GameListViewHolder(row);
    }

    @Override
    public void onBindViewHolder(GameListViewHolder holder, int position) {
        Game game = gamesList.get(position);
        holder.getName().setText(game.name);
    }

    @Override
    public int getItemCount() {
        return gamesList.size();
    }

    public void setGamesList(Games games) {
        this.games = games;
        gamesList = games.getData();
        notifyDataSetChanged();
    }


    public Game getSelected ( int position) {
        return games.data.get(position);
    }
}
