package com.bhupen.igtdemo.Features.GameList;

import com.bhupen.igtdemo.DataTypes.Game;


public interface GameListPresenterInterface {

    void loadGames();

    void loadPalyerInfo();

    void setView(GameListViewInterface view);

    void launchDetailsViews(Game game);
}
